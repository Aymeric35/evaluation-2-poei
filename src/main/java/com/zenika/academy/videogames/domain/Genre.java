package com.zenika.academy.videogames.domain;

public class Genre {
    private String name;

    public Genre(String name) {
        this.name = name;
    }

    public Genre() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
