package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService) {
        this.videoGamesService = videoGamesService;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     *
     * Exemple :
     *
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames() {
        return videoGamesService.ownedVideoGames();
    }

    /**
     * Récupérer un jeu vidéo par son ID
     *
     * Exemple :
     *
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) {
        VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id);
        if(foundVideoGame != null) {
            return ResponseEntity.ok(foundVideoGame);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public VideoGame addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {
        return this.videoGamesService.addVideoGame(videoGameName.getName());
    }

    /**
     * Marquer un jeu comme terminé
     *
     * Exemple :
     *
     * PUT /video-games/3561
     */
    @PutMapping("/{id}")
    public ResponseEntity<VideoGame> setGameStateToFinished(@PathVariable("id") Long id) {
        VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id);
        if (foundVideoGame != null) {
            foundVideoGame.setFinished(true);
            return ResponseEntity.ok(foundVideoGame);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Supprimer un jeu de la collection par son nom
     *
     * Exemple :
     *
     * DELETE /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "Spelunky"
     * }
     */
    @DeleteMapping
    public VideoGame deleteOneVideoGame(@RequestBody VideoGameNameRepresentation videoGameName) {
        return this.videoGamesService.removeVideoGame(videoGameName.getName());
    }
}
